package com.example.demo;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest(classes = {ContextTestBase.TestConfig.class, DemoApplication.class})
class ContextTestBase {

    @MockBean
    public static class TestConfig {

    }
}
