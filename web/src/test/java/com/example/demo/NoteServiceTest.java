package com.example.demo;

import com.example.demo.notes.NoteData;
import com.example.demo.notes.NoteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class NoteServiceTest extends ContextTestBase {

    @Autowired
    NoteService noteService;

    @Test
    void contextLoads() {
        String content = "content";

        NoteData noteData = new NoteData();
        noteData.setContent(content);

        NoteData savedNote = noteService.save(noteData);

        assertThat(savedNote).isNotNull();
        assertThat(savedNote.getContent()).isEqualTo(content);
        assertThat(savedNote.getId()).isNotNull();
        assertThat(savedNote.getCreatedAt()).isNotNull();
    }

}
