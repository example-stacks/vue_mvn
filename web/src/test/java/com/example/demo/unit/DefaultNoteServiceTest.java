package com.example.demo.unit;

import com.example.demo.notes.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultNoteServiceTest {

    @Mock
    NoteRepository noteRepository;
    @Mock
    NoteMapper noteMapper;

    @InjectMocks
    DefaultNoteService defaultNoteService;

    @Test
    public void shouldSaveNote() {
        String content = "content";
        long id = 123l;

        NoteData noteData = new NoteData();
        noteData.setContent(content);


        NoteData noteDataOutput = new NoteData();
        noteDataOutput.setContent(content);
        noteDataOutput.setId(id);
        noteDataOutput.setContent(content);

        NoteRecord noteRecord = new NoteRecord();
        noteRecord.setId(id);
        noteRecord.setContent(content);

        when(noteRepository.save(any(NoteRecord.class))).thenReturn(noteRecord);
        when(noteMapper.toNoteData(any(NoteRecord.class))).thenReturn(noteDataOutput);

        NoteData response = defaultNoteService.save(noteData);

        assertThat(response.getId()).isEqualTo(id);
        assertThat(response.getContent()).isEqualTo(content);
    }

}
