package com.example.demo.unit;

import com.example.demo.notes.NoteData;
import com.example.demo.notes.NoteMapper;
import com.example.demo.notes.NoteRecord;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class NoteMapperTest {

    private NoteMapper noteMapper = new NoteMapper();

    @Test
    public void shouldConvertNoteRecordToNote() {
        Long id = 123L;
        String content = "content";
        Date now = new Date();

        NoteRecord noteRecord = new NoteRecord();
        noteRecord.setContent(content);
        noteRecord.setCreatedAt(now);
        noteRecord.setId(id);

        NoteData noteData = noteMapper.toNoteData(noteRecord);

        assertThat(noteData.getContent()).isEqualTo(content);
        assertThat(noteData.getCreatedAt()).isEqualTo(now);
        assertThat(noteData.getId()).isEqualTo(id);
    }
}
