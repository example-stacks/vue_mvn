package com.example.demo.notes;

import java.util.Date;
import java.util.List;

public interface NoteService {
    NoteData save(NoteData noteData);

    List<NoteData> findNotes(Date createdBefore);

    NoteData getNote(Long id);
}
