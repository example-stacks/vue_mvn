package com.example.demo.notes;

import lombok.Data;

import java.util.Date;

@Data
public class NoteData {

    private Long id;

    private Date createdAt;

    private String content;
}
