package com.example.demo.notes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class DefaultNoteService implements NoteService {

    private static int DEFAULT_PAGE_SIZE = 2;

    private final NoteRepository noteRepository;
    private final NoteMapper noteMapper;

    @Override
    public NoteData save(NoteData noteData) {
        log.debug("Saving note: {}", noteData.getContent());

        val noteRecord = new NoteRecord();
        noteRecord.setContent(noteData.getContent());
        val savedNoteRecord = noteRepository.save(noteRecord);

        return noteMapper.toNoteData(savedNoteRecord);
    }

    @Override
    public List<NoteData> findNotes(Date createdBefore) {
        log.info("Querying notes createdAt before: {}", createdBefore);
        List<NoteRecord> notes = noteRepository.findByCreatedAtBeforeOrderByCreatedAtDesc(createdBefore, Pageable.ofSize(DEFAULT_PAGE_SIZE));

        return notes.stream()
                .map(noteMapper::toNoteData)
                .collect(Collectors.toList());
    }

    @Override
    public NoteData getNote(Long id) {
        val noteRecordOptional = noteRepository.findById(id);
        return noteMapper.toNoteData(noteRecordOptional.orElseThrow(NoteNotFoundException::new));
    }
}
