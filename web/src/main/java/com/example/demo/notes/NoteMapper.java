package com.example.demo.notes;

import lombok.val;
import org.springframework.stereotype.Service;

@Service
public class NoteMapper {

    public NoteData toNoteData(NoteRecord noteRecord) {
        val note = new NoteData();
        note.setContent(noteRecord.getContent());
        note.setCreatedAt(noteRecord.getCreatedAt());
        note.setId(noteRecord.getId());

        return note;

    }

    public NoteRecord toNoteRecord(NoteData noteData) {
        val note = new NoteRecord();
        note.setContent(noteData.getContent());
        note.setCreatedAt(noteData.getCreatedAt());
        note.setId(noteData.getId());

        return note;
    }
}
