package com.example.demo.notes;

import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/notes")
@AllArgsConstructor
public class NoteController {

    private final NoteService noteService;

    @GetMapping(params = {"createdBefore"})
    public List<NoteData> findNotes(@RequestParam("createdBefore") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date createdBefore) {
        return noteService.findNotes(createdBefore);
    }

    @PostMapping
    public NoteData save(@RequestBody NoteData noteData) {
        return noteService.save(noteData);
    }

    @GetMapping("/{id}")
    public NoteData get(@PathVariable("id") Long id) {
        return noteService.getNote(id);
    }
}
