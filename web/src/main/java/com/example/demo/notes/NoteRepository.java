package com.example.demo.notes;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface NoteRepository extends CrudRepository<NoteRecord, Long> {

    List<NoteRecord> findByCreatedAtBeforeOrderByCreatedAtDesc(Date createdAt, Pageable pageable);

}
