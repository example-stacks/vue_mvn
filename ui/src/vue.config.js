module.exports = {
    outputDir: '../target/dist',
    devServer: {
        port: 3434,
        proxy: {
            '^/api': {
                target: 'http://localhost:8080',
            }
        }
    }
}
