import NoteCreateForm from '@/components/NoteCreateForm.vue'
import {shallowMount} from '@vue/test-utils'

import {createStore} from 'vuex'
import {NOTE_SAVE} from "../../src/store/modules/notes.module";


describe('NoteCreateForm.vue', () => {
    it('calls store on form send', async () => {
        const $store = {
            'notes/': {
                dispatch: jest.fn()
            }
        }

        const store = createStore({
            modules: {
                notes: {
                    namespaced: true,
                    state() {
                        return {
                            count: 0
                        }
                    },
                    actions: {
                        [NOTE_SAVE]({state}) {
                            console.log('has been called')
                            state.count += 1
                        }
                    }
                }
            }
        })


        const wrapper = shallowMount(NoteCreateForm, {
            global: {
                plugins: [store]
            }
        })

        await wrapper.find('button').trigger('click')
        expect(store.state.notes.count).toBe(1)
    })
})
