import { createStore } from 'vuex'
import notes from './modules/notes.module';

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    notes
  }
})
