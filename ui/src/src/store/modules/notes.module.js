import {notesApi} from '../../api/'

export const NOTES_FETCH = 'notesFetch';
export const NOTE_SAVE = 'noteSave';

export const RESET_STATE = 'resetState';
export const START_LOADING = 'startLoading';
export const STOP_LOADING = 'stopLoading';

export const SET_NOTES = 'setNotes';
export const ADD_NOTE = 'addNote';


const getDefaultState = () => {
    return {
        notes: [],
        loading: false,
        loadedAll: false,
        lastEvaluatedKey: new Date().toISOString()
    }
}

export const state = getDefaultState();

const actions = {
    async [NOTES_FETCH]({commit, state}) {
        try {
            if (!state.loadedAll) {
                commit(START_LOADING);
                const response = await notesApi.list(state.lastEvaluatedKey);
                commit(SET_NOTES, response);
            }
        } finally {
            commit(STOP_LOADING);
        }
    },

    async [NOTE_SAVE]({commit}, data) {
        try {
            commit(START_LOADING);
            const response = await notesApi.save(data);
            commit(ADD_NOTE, response);
        } finally {
            commit(STOP_LOADING);
        }
    },
};

export const mutations = {
    [START_LOADING](state) {
        state.loading = true;
    },
    [STOP_LOADING](state) {
        state.loading = false;
    },
    [SET_NOTES](state, items) {
        state.notes.push(...items);
        state.loadedAll = !items.length;
        if (items.length) {
            state.lastEvaluatedKey = items[items.length - 1].createdAt;
        }
    },
    [ADD_NOTE](state, note) {
        state.notes.unshift(note)
    },
    [RESET_STATE](state) {
        Object.assign(state, getDefaultState())
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
