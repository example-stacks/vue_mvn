import axios from 'axios'

function NotesApi() {

    const axiosInstance = axios.create({
        baseURL: '/api/notes',
    });

    const get = async (id) => {
        return axiosInstance.get(`/${id}`)
            .then(response => response.data);
    };

    const list = async (createdBefore) => {
        return axiosInstance.get(``, {
            params: {
                createdBefore
            }
        }).then(response => response.data);
    };

    const save = async (body) => {
        return axiosInstance.post('', body)
            .then(response => response.data);
    };

    return {
        list,
        get,
        save
    }
}

export default NotesApi
